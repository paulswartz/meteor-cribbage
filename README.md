Cribbage
========

Online Cribbage, implemented with [Meteor](http://meteor.com).


Running
-------

After installing [Meteorite](http://oortcloud.github.io/meteorite/):

    $ git clone https://github.com/paulswartz/meteor-cribbage.git
    $ cd meteor-cribbage
    $ mrt

Acknowledgements
----------------

I got the CSS for playing cards from selfthinker's [CSS-Playing-Cards](https://github.com/selfthinker/CSS-Playing-Cards), licensed under CC-BY-SA 3.0.