Meteor.startup ->
    Meteor.publish "game", (slug) ->
        Games.find
            slug: slug
            players: @userId

    Meteor.publish "hand", (slug) ->
        Cards.find
            game: slug
            player: @userId

    Meteor.publish "crib", (slug) ->
        Cards.find
            game: slug
            player: "crib"
        ,
            fields:
                suit: 0
                rank: 0
                display: 0
                value: 0
