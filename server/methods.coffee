shuffled_deck = ->
    deck = []
    for suit in ["clubs", "diams", "hearts", "spades"]
        for rank in ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
            deck.push
                suit: suit
                rank: rank

    deck.sort (a, b) ->
        Math.random() - 0.5
    deck

Meteor.methods
    login: (user_id) ->
        user_id = Random.id() unless user_id
        @setUserId user_id
        @userId

    createGame: ->
        digits = 4
        slug = Random.hexString(digits).toUpperCase()
        slug = Random.hexString(digits).toUpperCase() while Games.find(slug: slug).count()
        Games.insert
            slug: slug
            state: "waiting"
            players: [@userId]
        slug

    joinGame: (game_id) ->
        return if Games.find(
            slug: game_id
            players: @userId
        ).count()
        Games.update
            slug: game_id
            state: "waiting"
        ,
            $set:
                state: "deal"
            $push:
                players: @userId
        , (error) ->
            return Meteor.Router.to "/" if error
            game = Games.findOne
                slug: game_id
            Games.update
                slug: game_id
            ,
                $set:
                    dealer: game.players[Math.floor Math.random() * 2]
                    cards: null

        Meteor.call "deal", game_id

    deal: (game_id) ->
        game = Games.findOne
            slug: game_id
            state: "deal"
        return if not game
        # return if Cards.find(
        #     game: game_id).count()
        Cards.remove
            game: game_id
        deck = shuffled_deck()
        other_hand = (deck[i] for i in [0..10] by 2)
        dealer_hand = (deck[i] for i in [1..11] by 2)
        deal_card = (player, card) ->
            switch card.rank
                when "A" then value = 1
                when "J", "Q", "K" then value = 10
                else value = parseInt(card.rank)
            Cards.insert
                game: game_id
                player: player
                suit: card.suit
                rank: card.rank
                display: card.rank + card.suit[0].toUpperCase()
                value: value
        for player in game.players
            hand = if player is game.dealer then dealer_hand else other_hand
            deal_card player, card for card in hand

        deal_card "top", deck[12]

    toCrib: (game_id) ->
        Cards.update
            game: game_id
            player: @userId
            selected: true
        ,
            $set:
                player: "crib"
            $unset:
                selected: true
        ,
            multi: true
        if Cards.find(
            game: game_id
            player: "crib").count() is 4
            Game.update
                slug: game_id
                state: "deal"
            ,
                state: "game"
