@currentGameId = ->
    Session.get "currentGame"

@currentGame = ->
    Games.findOne
        slug: currentGameId()

@userId = ->
    amplify.store "user_id"
