Meteor.Router.add
    "/": "index"
    "/join": "join"
    "/:game_id": (game_id) ->
        Meteor.subscribe "game", game_id
        Meteor.subscribe "hand", game_id
        Meteor.subscribe "crib", game_id
        Session.set "currentGame", game_id
        game = currentGame()
        if !game or game.state == "waiting"
            "waiting"
        else
            "game"
    Template.index.events
      "click #create": ->
          Meteor.call "createGame", (error, result) ->
              Meteor.Router.to "/" + result  unless error

      "click #join": ->
          Meteor.Router.to "join"

    Template.join.events "submit form": (ev) ->
        ev.preventDefault()
        game_id = $("#game_id").val()
        Meteor.call "joinGame", game_id, (error, result) ->
            Meteor.Router.to "/" + game_id    unless error
        false

    Template.game.events
        "click .card": ->
            Cards.update
                _id: @_id
            ,
                $set:
                    selected: !@selected

        "click #to_crib": ->
            Meteor.call "toCrib", currentGameId()
            Session.set "sentCrib", true

    Template.game.selectedCount = (count) ->
        return Cards.find(
            selected: true
        ).count() is count

    Template.game.sentCrib = ->
        Session.equals "sentCrib", true

    Template.card.title = (c) ->
        switch c
            when "clubs" then "Clubs"
            when "diams" then "Diamonds"
            when "hearts" then "Hearts"
            when "spades" then "Spades"
            when "A" then "Ace"
            when "J" then "Jack"
            when "Q" then "Queen"
            when "K" then "King"
            else c

    Meteor.startup ->
        Meteor.call "login", userId(), (error, result) ->
            amplify.store "user_id", result

Handlebars.registerHelper "game", currentGame

Handlebars.registerHelper "userId", userId

Handlebars.registerHelper "hand", () ->
    Cards.find
        game: currentGameId()
        player: userId()

Handlebars.registerHelper "player_name", ->
    if this.toString() is userId()
        "You"
    else
        "Them"

Handlebars.registerHelper "dealer", ->
    game = currentGame()
    this.toString() is game.dealer

Handlebars.registerHelper "score", ->
    game = currentGame()
    (game.scores?[this.toString()] | 0).toString()

Handlebars.registerHelper "games", ->
    game = currentGame()
    (game.games?[this.toString()] | 0).toString()
